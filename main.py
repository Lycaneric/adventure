import utilities as util
import rooms


# check for save file
def start_up():
    # check for save file
    player = util.load_game()
    # if not save then make player and save
    if not player:
        player = util.name_player()
        util.save_game(player)
        main_loop(player)
    # else enter the game
    else:
        print("Welcome back, %s." % player['name'])
        main_loop(player)


# main loop for game
def main_loop(player):
    current_room = player['current_room']
    # if no current room set to start and add player attributes
    if current_room == '':
        current_room = player['current_room'] = 'start_room'
        player['keys'] = []
        player['weapon'] = {}
        player['items'] = []
        util.save_game(player)
        getattr(rooms, current_room)(player)
    else:
        getattr(rooms, current_room)(player)
