import json


# get the players name
def name_player():
    player = {}
    name = input("What is your name? ")
    player['name'] = name
    player['current_room'] = ''
    print("So, you're %s? " % player['name'])
    confirm()
    return player


# confirm yes or no
def confirm():
    unanswered = True
    while unanswered:
        answer = input("yes or no? (y/n) ")
        answer = answer.lower()
        if answer == 'y' or answer == 'yes':
            unanswered = False
            answer = 1
            return answer
        elif answer == 'n' or answer == 'no':
            unanswered = False
            answer = 0
            return answer
        else:
            print("That is not an acceptable answer...")


# pick a wall to check
def wall_choice():
    print("""
        Which wall would you like to check?

        """)
    unanswered = True
    while unanswered:
        choice = input("(North, East, South, or West:) ")
        choice = choice.lower()
        if choice in ('n', 'north', 'e', 'east', 's', 'south', 'w', 'west'):
            unanswered = False
            return choice
        else:
            print("That is not an acceptable answer...")


# process combat actions
def combat():
    return


# save file
def save_game(player):
    try:
        with open('save.txt', 'w') as outfile:
            json.dump(player, outfile)
            print("Game saved!")
    except:
        print("Unable to save the game.")


# load file
def load_game():
    try:
        with open('save.txt') as infile:
            player = json.load(infile)
            return player
    except:
        print("No save file present.")
        return False


# process the room
def process_room(player, room):
    # print the description for the room
    print(room['description'])

    if not room['enemies']:
        wall = wall_choice()
        wall_check(wall, room, player)
    else:
        combat()


# after wall selection, send player to check objects
def wall_check(wall, room, player):
    if wall in ('n', 'north'):
        wall = 'north_wall'
        print('Checking north wall...')
        check_objects(wall, room, player)
    elif wall in ('e', 'east'):
        wall = 'east_wall'
        print('Checking east wall...')
        check_objects(wall, room, player)
    elif wall in ('s', 'south'):
        wall = 'south_wall'
        print('Checking south wall...')
        check_objects(wall, room, player)
    elif wall in ('w', 'west'):
        wall = 'west_wall'
        print('Checking west wall...')
        check_objects(wall, room, player)


# roll through objects in the room based on wall selection
def check_objects(wall, room, player):
    from main import main_loop

    # add to this for other table like things
    # need other lists for other object types
    item_tables = ['table', 'desk']

    # if there are objects on the wall
    if room['objects']['{0}'.format(wall)]:
        stuff_exists = room['objects']['{0}'.format(wall)]
        # iterate through objects on wall
        for item in stuff_exists:
            # object is a door
            if item == 'door':
                print('There is a door on this wall.')
                print("""
                Would you like to try the door?
                      """)
                answer = confirm()
                if answer == 1:
                    # if the door is locked you'll need a key
                    if room['objects']['{0}'.format(wall)]['door']['locked']:
                        if room['has_key']:
                            print('You have the key!')
                            print('You have unlocked the door!')
                            # send player to next room
                            next_room = (room['objects']['{0}'.format(wall)]
                                         ['door']['next_room'])
                            player['current_room'] = next_room
                            save_game(player)
                            main_loop(player)
                        # no key
                        else:
                            print("The door is locked, you'll need a key.")
                            wall = wall_choice()
                            wall_check(wall, room, player)
                    else:
                        # send player to next room
                        next_room = (room['objects']['{0}'.format(wall)]
                                     ['door']['next_room'])
                        player['current_room'] = next_room
                        save_game(player)
                        main_loop(player)
                # not checking the door
                else:
                    wall = wall_choice()
                    wall_check(wall, room, player)
            # object not a door
            else:
                print('You see a {0}...'.format(item))
                print(room['objects']['{0}'.format(wall)]['{0}'.format(item)]
                      ['description'])
                # if item in list of table like objects
                if item in item_tables:
                    print('Do you want to check the drawer?')
                    answer = confirm()
                    # open the drawer
                    if answer == 1:
                        print('You open the drawer...')
                        # show discription
                        print(room['objects']['{0}'.format(wall)]
                              ['{0}'.format(item)]['drawer']['description'])
                        # if object has loot
                        if (room['objects']['{0}'.format(wall)]
                                ['{0}'.format(item)]['drawer']['loot']):
                            print('There is loot!')
                        else:
                            print('No loot.')
                # item not in other lists, go back to wall choice for now
                wall = wall_choice()
                wall_check(wall, room, player)
    # nothing cool on the wall
    else:
        print("""
            This wall has nothing of interest
            """)
        wall = wall_choice()
        wall_check(wall, room, player)
