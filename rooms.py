import utilities as util


# start room
def start_room(player):
    # room dictionary
    room = {}
    # room info
    room['name'] = 'start_room'
    room['enemies'] = []
    room['description'] = """
    The room has a musty smell to it. The lighting is low but it's
    bright enough you can see. On the north wall you see a door that is the
    source of the light in the room. On the east wall you see a rather
    destroyed cot.
    """
    # objects contain walls which have their own objects
    room['objects'] = {
        'north_wall': {
            'door': {
                'description': """
                The door is shoddy, it's old and splintered wood that looks
                like it would be easy to bust through. There is a barred
                window on the top half of the door.
                """,
                'locked': False,
                'next_room': 'a1',
            },
        },
        'east_wall': {},
        'south_wall': {},
        'west_wall': {
            'table': {
                'description': """
                The table is a mess. There are various papers and envelopes
                everywhere. You notice that the names and addresses are all
                blacked out. It all looks to be junk mail, nothing really
                of interest. You also see that the table has a drawer.
                """,
                'has_drawer': True,
                'drawer': {
                    'description': """
                    stuff
                    """,
                    'loot': [],
                }
            }
        },
    }
    # check if player has a key to the room
    if room['name'] in player['keys']:
        room['has_key'] = True
    else:
        room['has_key'] = False
    # tell player what room they entered.
    print("{0} has entered {1}".format(player['name'], room['name']))
    # process room
    util.process_room(player, room)


# room a1
def a1(player):
    print('a1')
